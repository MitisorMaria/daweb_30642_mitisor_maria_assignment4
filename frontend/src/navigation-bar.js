import React from 'react'
import logo from './commons/images/sigla.png';


import {
    Nav,
    Navbar,
    NavbarBrand,
    NavLink
} from 'reactstrap';

import counterpart from "counterpart";
import Translate from "react-translate-component";
import eng from "./lang/eng";
import ro from "./lang/ro";
import Button from "react-bootstrap/Button";
import {Redirect} from "react-router-dom";
import {HOST} from "./commons/hosts";

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

counterpart.registerTranslations('eng', eng);
counterpart.registerTranslations('ro', ro);

class NavigationBar extends React.Component {
    state = {
        redirect : false
    };

    constructor(props){
        super(props);
        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        const lang = localStorage.getItem("lang");
        if(lang) {
            this.setState({ lang: lang});
            counterpart.setLocale(lang);
        }
        else{
            this.setState({ lang: 'ro'});
            counterpart.setLocale(lang);
        }
    }
    componentDidUpdate(prevProps, prevState) {

    }

    onLangChange = (e) => {
        this.setState({lang: e.target.value});
        localStorage.setItem("lang", e.target.value);
        counterpart.setLocale(localStorage.getItem("lang"));
    }

    logout(){
        sessionStorage.clear();
        const xmlhttp = new XMLHttpRequest();
        const theUrl = HOST.laravel_api + "logout/";
        xmlhttp.open("POST", theUrl, false);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xmlhttp.onload = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200) {
                window.alert("Logged out!");
            } else {
                window.alert("Error " + xmlhttp.status);
            }
        }
        xmlhttp.send();
    }

    render() {

        if (!sessionStorage.getItem("username") && !sessionStorage.getItem("register")) {
            return (<Redirect to={'/'}/>)
        }

        if (!sessionStorage.getItem("username") && sessionStorage.getItem("register")) {
            return (<Redirect to={'/register'}/>)
        }
        return (
            <div>
                <Navbar color="dark" light expand="md">
                    <NavbarBrand href="/">
                        <img src={logo} width={"50"}
                             height={"35"} />
                    </NavbarBrand>

                    <Nav className="mr-auto" navbar>
                        <ul style={textStyle} className='nav navbar-nav navbar-inverse navbar-custom'>
                            <li><Translate content="acasa" component="a" class="nav-link" href="/acasa" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="noutati" component="a" class="nav-link" href="/noutati" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="despre" component="a" class="nav-link" href="/despre" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="profil" component="a" class="nav-link" href="/profilStudent" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="coord" component="a" class="nav-link" href="/coordonator" style={textStyle} unsafe={true}/></li>
                            <li><Translate content="profilulMeu" component="a" class="nav-link" href="/pagProfil" style={textStyle} unsafe={true}/></li>
                            <li><NavLink href="/contact" style={textStyle}>Contact</NavLink></li>
                        </ul>
                    </Nav>
                    <Button name="logout" href="/" onClick={this.logout}>Logout</Button>
                    <select  value={this.state.lang} onChange={this.onLangChange}>
                        <option value="eng">ENG</option>
                        <option value="ro">RO</option>
                    </select>
                </Navbar>
            </div>

        )
    }
}

export default NavigationBar
