import React from 'react';
import style from './commons/styles/style.css'
import BackgroundImg from './commons/images/poza.jpg';
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', align: 'middle'};
class Coordonator extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
    }



    refresh(){
        this.forceUpdate()
    }

    render() {
        return (
            <div  style={backgroundStyle}>
                <h1 style={textStyle}>Coordonator</h1>
                <p style={textStyle}><b>Nume și prenume: </b>Mărginean Anca</p>
                <p style={textStyle}><b>Funcție: </b>conferențiar</p>
                <p style={textStyle}><b>Direcții de cercetare: </b>
                    <ul>
                        <li>Knowledge representation</li>
                        <li>Machine Learning</li>
                        <li>Natural Language Processing</li>
                    </ul>
                </p>
                <p class={style.right}><img src ={require ('./commons/images/marginean.jpg') }/></p>
                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Coordonator;
