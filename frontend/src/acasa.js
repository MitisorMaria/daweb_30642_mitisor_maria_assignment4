import React from 'react';
import BackgroundImg from './commons/images/poza.jpg';
import {Button, Container} from "reactstrap";
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', align: 'middle'};
class Acasa extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
    }



    refresh(){
        this.forceUpdate()
    }

    render() {
        return (
            <div  style={backgroundStyle}>
                <Container fluid>
                    <h1 className="display-3" style={textStyle}>Recunoașterea vorbitorului dintr-o înregistrare</h1>
                    <p className="lead" style={textStyle}> <b>Lucrare de licență a studentei Mitișor Maria-Alexandra de la secția de Tehnologia
                    Informației a Facultății de Automatică și Calculatoare a UTCN</b> </p>
                    <hr className="my-2"/>
                    <p  style={textStyle}> <b>Coordonator: prof. Anca Mărginean </b> </p>
                </Container>
                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Acasa;
