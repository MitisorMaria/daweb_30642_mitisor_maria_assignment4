import React from 'react';
import BackgroundImg from './commons/images/poza.jpg';
import {Card, Col, Row} from 'reactstrap';
import { useTable } from "react-table";

import CanvasJSReact from './canvasjs.react';
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "130%",
    backgroundImage: `url(${BackgroundImg})`
};

const backgroundImgStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "130%",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', align: 'middle'};

const Table = ({ columns, data }) => {
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = useTable({
        columns,
        data
    });

    return (
        <table {...getTableProps()}>
            <thead>
            {headerGroups.map(headerGroup => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                    {headerGroup.headers.map(column => (
                        <th {...column.getHeaderProps()}>{column.render("Header")}</th>
                    ))}
                </tr>
            ))}
            </thead>
            <tbody {...getTableBodyProps()}>
            {rows.map((row, i) => {
                prepareRow(row);
                return (
                    <tr {...row.getRowProps()}>
                        {row.cells.map(cell => {
                            return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
                        })}
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
};


const columns = [
    {
        Header: 'Comment',
        accessor: 'comment'
    },
    {
        Header: 'Date',
        accessor: 'date'
    },
    {
        Header: 'Username',
        accessor: 'username'
    },
]

class Despre extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.fetchComments = this.fetchComments.bind(this);
        this.getArray = this.getArray.bind(this);
        this.addSymbols = this.addSymbols.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,
            comment: '',
            tableData: new Array(),
            daysArray: []
        };
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    getArray(){
        const xmlhttp = new XMLHttpRequest();
        const theUrl = `http://localhost:8000/api/comments/getCommentsByDay`;
        xmlhttp.open("GET", theUrl, false);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        let array=[];
        xmlhttp.onload = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200 && xmlhttp.response) {
                const obj= JSON.parse(xmlhttp.response);
                array = obj.array;
            } else {
                window.alert("Error " + xmlhttp.status);
            }
        }
        xmlhttp.send();
        this.setState({daysArray : array});
    }

    handleSubmit(event) {
        const comment = this.state.comment;
        const username = sessionStorage.getItem("username");
        const xmlhttp = new XMLHttpRequest();
        const theUrl = `http://localhost:8000/api/comments/`;
        xmlhttp.open("POST", theUrl, false);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xmlhttp.onload = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200 && xmlhttp.response) {
                window.alert("Comment added successfully!");
            } else {
                window.alert("Error " + xmlhttp.status);
            }
        }
        xmlhttp.send(JSON.stringify({"comment": comment, "username": username}));
    }

    refresh(){
        this.forceUpdate();
    }

    componentDidMount() {
        this.fetchComments();
        this.getArray();
    }

    fetchComments() {
        const xmlhttp = new XMLHttpRequest();
        let theUrl =  `http://localhost:8000/api/comments/`;
        xmlhttp.open("GET", theUrl, false);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");


        let tableData = new Array();
        xmlhttp.onload = function () {
            let result = JSON.parse(xmlhttp.response);
            let status = xmlhttp.status;

            if(xmlhttp.readyState === XMLHttpRequest.DONE && result !== null && status === 200) {
                result.forEach( x => {
                    tableData.push({
                        comment: x.comment,
                        username: x.username,
                        date: x.date
                    });
                });
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
            }
        }

        xmlhttp.send();
        this.setState({tableData: tableData});

    }

    addSymbols(e){
        var suffixes = ["", "K", "M", "B"];
        var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000000000000)), 0);
        if(order > suffixes.length - 1)
            order = suffixes.length - 1;
        var suffix = suffixes[order];
        return CanvasJS.formatNumber(e.value / Math.pow(1000000000000, order)) + suffix;
    }


    render() {
        let currentdate = new Date();
        let today = currentdate.getDate();
        const options = {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Comentarii in ultimele 5 zile"
            },
            axisX: {
                title: "Zi",
                reversed: true,
            },
            axisY: {
                title: "Numar de comentarii",
                labelFormatter: this.addSymbols
            },
            data: [{
                type: "bar",
                dataPoints: [
                    { y:  this.state.daysArray[today], label: "Azi" },
                    { y:  this.state.daysArray[today-1], label: "Ieri" },
                    { y:  this.state.daysArray[today-2], label: "Alaltăieri" },
                    { y:  this.state.daysArray[today-3], label: "Acum 4 zile" },
                    { y:  this.state.daysArray[today-4], label: "Acum 5 zile" }
                ]
            }]
        }


        return (
            <div  style={backgroundStyle}>
                <h2 style={textStyle}>Comentarii:</h2>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        <input type="text" name="comment" value={this.state.comment}  placeholder={"Enter comment"} onChange={this.handleChange} />
                    </label>

                    <input type="submit" value="Submit"/>

                </form>
                <Row  style={backgroundStyle}>
                    <Col>
                        <Card body>
                            <Table  style={backgroundStyle}
                                    data={this.state.tableData}
                                    columns={columns}
                            />
                        </Card>
                    </Col>
                </Row>
                <CanvasJSChart options = {options} style={backgroundImgStyle}/>

                <h1 style={textStyle}>Despre lucrare</h1>

                <p  style={textStyle}> <b>Lucrarea consta in crearea unei aplicatii care sa poata identifica persoana care
                vorbeste intr-o inregistrare. Acest lucru va fi realizat cu ajutorul unei retele neuronale recurente.
                Inregistrarile din setul de date de antrenament, precum si cele din setul de test vor avea un singur vorbitor.
                    Tehnologia folosita este TensorFlow.</b></p>
                <p  style={textStyle}> <b>Exista mai multe tipuri de recunoastere de vorbitor, si anume:
                    <ul>
                        <li>
                            In functie de ce vorbitori avem: closed-set (luam in calcul doar cativa vorbitori pe care ii avem deja in baza de date)
                            sau open-set (daca dam peste o inregistrare a unui vorbitor necunoscut, il adaugam la restul vorbitorilor);
                        </li>
                        <li>
                            In functie de ceea ce se vorbeste: text-dependent sau text-independent;
                        </li>
                    </ul>

                    Aplicatia pe care o vom dezvolta este closed-set si text-dependent.
                </b> </p>



                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Despre;
