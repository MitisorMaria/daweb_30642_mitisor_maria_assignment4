import React from 'react';
import BackgroundImg from './commons/images/poza.jpg';
import {Button, Container} from "reactstrap";
import {Redirect} from 'react-router-dom';
import {HOST} from "./commons/hosts";
import Profil from "./pagProfil";
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', align: 'middle'};
class Login extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            username: "",
            password: "",
            redirect: false,
            register: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.login = this.login.bind(this);
        this.registerUser = this.registerUser.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
    }


    login() {
        const username = this.state.username;
        const password = this.state.password;
        if (username && password) {
            const xmlhttp = new XMLHttpRequest();
            const theUrl = HOST.laravel_api + "validate/";
            xmlhttp.open("POST", theUrl, false);
            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xmlhttp.setRequestHeader("Cookie", "username="+username);


            xmlhttp.onload = function () {
                if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200 && xmlhttp.response) {
                    sessionStorage.setItem('username', username);
                    sessionStorage.setItem('password', password);
                    this.setState({redirectToReferrer: true});
                } else {
                    window.alert("Error " + xmlhttp.status);
                }
            }
            xmlhttp.send(JSON.stringify({"username": username, "password": password}));
        }
    }

    refresh(){
        this.forceUpdate()
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    registerUser(){
        this.setState({register : true});
        sessionStorage.setItem('register', 'true');
    }


    render() {
        if (this.state.redirectToReferrer) {
            return (<Redirect to={'/contact'}/>)
        }

        if(sessionStorage.getItem('username') && sessionStorage.getItem('password')){
            return (<Redirect to={'/pagProfil'}/>)
        }

        return (
            <div  style={backgroundStyle}>
                <Container fluid>
                    <h1 className="display-3" style={textStyle}>Login</h1>
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            <input type="text" name="username" value={this.state.username}  placeholder={"username"} onChange={this.handleChange} />
                        </label>

                        <label>
                            <input type="password" name="password" value={this.state.password}  placeholder={"password"} onChange={this.handleChange} />
                        </label>

                        <input type="submit" value="Login" onClick={this.login}/>
                    </form>
                    <p style={textStyle}>Nu aveți cont?<a href = "/register" onClick={this.registerUser}> Creați unul.</a></p>
                </Container>
                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Login;
