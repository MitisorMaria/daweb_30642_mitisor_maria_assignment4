import React from 'react';
import counterpart from 'counterpart';
import BackgroundImg from './commons/images/poza.jpg';
import Translate from 'react-translate-component';
import eng from './lang/eng';
import ro from './lang/ro';
import axios from 'axios';
import {HOST} from './commons/hosts';


import './person-data/person/fields/fields.css';
import Button from "react-bootstrap/Button";


const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`,
    display: 'flex',
    flexDirection: 'column'
};

const textStyle = {color: 'white', align: 'middle'};

counterpart.registerTranslations('eng', eng);
counterpart.registerTranslations('ro', ro);
counterpart.setLocale(localStorage.getItem("lang"));



class Edit extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null,
            lang: "ro",
            mail: "",
            password: "",
            image:''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleUpload = this.handleUpload.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    componentDidMount() {
        const lang = localStorage.getItem("lang");
        if(lang) {
            this.setState({ lang: lang});
            counterpart.setLocale(lang);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const lang = localStorage.getItem("lang");
        counterpart.setLocale(lang);
    }


    createImage(file) {
        let reader = new FileReader();
        reader.onload = (e) => {
            this.setState({
                image: e.target.result
            })
        };
        reader.readAsDataURL(file);
    }

    handleUpload(event) {
        let files = event.target.files || event.dataTransfer.files;
        if (!files.length)
            return;
        const image = this.createImage(files[0]);
    }

    handleChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit(event) {
        const email = this.state.mail;
        const username = sessionStorage.getItem("username");
        const password = this.state.password;
        const xmlhttp = new XMLHttpRequest();
        const theUrl = HOST.laravel_api;
        const image = this.state.image;
        xmlhttp.open("PUT", theUrl, false);
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xmlhttp.onload = function () {
            if (xmlhttp.readyState === XMLHttpRequest.DONE && xmlhttp.status === 200 && xmlhttp.response) {
                window.alert("User updated successfully!");
            } else {
                window.alert("Error " + xmlhttp.status);
            }
        }
        xmlhttp.send(JSON.stringify({"email": email, "username": username, "password":password, "image": image}));
    }

    render(){
        return (
            <div  style={backgroundStyle}>
                <h1 style={textStyle}>Editare informații</h1>


                <form onSubmit={this.handleSubmit}>
                    <label>
                        <input type="text" name="mail" value={this.state.mail}  placeholder={"Enter new e-mail address"} onChange={this.handleChange} />
                    </label>

                    <label>
                        <input type="password" name="password" value={this.state.password}  placeholder={"Enter new password"} onChange={this.handleChange} />
                    </label>


                    <input type="file" onChange={this.handleUpload} />
                    <input type="submit" value="Submit"/>

                    <p>
                        <Button name="register" href={"/pagProfil"}>Înapoi la profil</Button>
                    </p>
                </form>


                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Edit;
