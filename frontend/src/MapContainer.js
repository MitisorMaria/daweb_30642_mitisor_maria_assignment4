import React, { Component } from 'react';
import { withScriptjs } from "react-google-maps";
import MyMap from './Map';

const MapLoader = withScriptjs(MyMap);
class MapContainer extends Component  {

    render(){
        return (
            <div>
                <MapLoader
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHofKbW7iOW1SUiI5wfPBh8VJNg4l5SkE"
                    loadingElement={<div style={{ height: `100%` }} />}
                />
            </div>
        );
    }

};

export default MapContainer;