import React, { Component } from "react";
import {
    withGoogleMap,
    withScriptjs,
    GoogleMap,
    DirectionsRenderer
} from "react-google-maps";


class MyMap extends Component {
    state = {
        directions: null
    };

    componentDidMount() {
        const google=window.google;
        const directionsService = new google.maps.DirectionsService();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                let pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                 localStorage.setItem("pos", JSON.stringify(pos));
            }, function() {
                alert("error");
            });
        } else {
            // Browser doesn't support Geolocation
            alert("Browser doesn't support Geolocation");
        }


        let pos = localStorage.getItem("pos");
        const lat = JSON.parse(pos).lat;
        const lng = JSON.parse(pos).lng;

        const origin = { lat: lat, lng: lng };
        const destination = { lat: 46.772550, lng: 23.585630 };

        directionsService.route(
            {
                origin: origin,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING
            },
            (result, status) => {
                if (status === google.maps.DirectionsStatus.OK) {
                    this.setState({
                        directions: result
                    });
                } else {
                    console.error(`error fetching directions ${result}`);
                }
            }
        );
    }

    render() {
        const GoogleMapExample = withGoogleMap(props => (
            <GoogleMap
                defaultCenter={{ lat: 46.773550, lng: 23.585630 }}
                defaultZoom={13}
            >
                <DirectionsRenderer
                    directions={this.state.directions}
                />
            </GoogleMap>
        ));

        return (
            <div>
                <GoogleMapExample
                    containerElement={<div style={{ height: `500px`, width: "500px" }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                />
            </div>
        );
    }
}

export default MyMap;
