<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $comments = Comment::all();
        return response()->json($comments);
    }


    public function getNumberOfCommentsByDay(){
        $host = '127.0.0.1';
        $db   = 'daw';
        $user = 'root';
        $pass = 'paroladelamysql';
        $port = "3306";
        $charset = 'utf8mb4';

            $options = [
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            $dsn = "mysql:host=$host;dbname=$db;charset=$charset;port=$port";

            try {
                $pdo = new \PDO($dsn, $user, $pass, $options);
            } catch (\PDOException $e) {
                throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
        $array=[];
        $stmt = $pdo->query("SELECT COUNT(*) as countComments, EXTRACT(DAY FROM date) as dayDate FROM daw.comments group by EXTRACT(DAY FROM date)");
        while ($row = $stmt->fetch()) {
            $array[$row['dayDate']]=$row['countComments'];
        }
        if($array != null){
            return response()->json([
                'array' => $array]);
        }else{
            return response()->json(['message'=> 'error']);
        }

}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $now = 'NOW';
        $d = NOW();
        $inputArray      =           array(
            'username'        =>      $request->username,
            'comment'             =>      $request->comment,
            'date' => $d
        );
        $comment = Comment::create($inputArray);
        return response()->json(['message'=> 'comment created',
            'comment' => $comment]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
